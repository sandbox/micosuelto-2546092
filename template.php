<?php

/**
 * @file
 * Template overrides as well as (pre-)process and alter hooks for the
 * PORTA theme.
 */
function cornelius_preprocess_html(&$variables) {
  if (!empty($variables['page']['sidebar_first'])) {
    $variables['classes_array'][] = 'with_sidebar';
  }
  else {
    $variables['classes_array'][] = 'without_sidebar';
  }
}

function cornelius_status_messages($variables) {
  $display = $variables['display'];
  $output = '';

  $status_heading = array(
    'status' => t('Status message'),
    'error' => t('Error message'),
    'warning' => t('Warning message'),
  );
  foreach (drupal_get_messages($display) as $type => $messages) {
    $output .= '<div class="messages messages-wrap messages--' . $type . '">';
    $output .= '<div class="messages-grid l-width-grid">';
    if (!empty($status_heading[$type])) {
      $output .= '<h2 class="element-invisible">' . $status_heading[$type] . "</h2>";
    }
    if (count($messages) > 1) {
      $output .= '<ul>';
      foreach ($messages as $message) {
        $output .= '  <li>' . $message . '</li>';
      }
      $output .= '</ul>';
    }
    else {
      $output .= $messages[0];
    }
    $output .= '</div>';
    $output .= '</div>';
  }

  return $output;
}