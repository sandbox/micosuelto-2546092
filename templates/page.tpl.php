<div class="l-page">
  
<!-- PAGE TOP REGION -->

          <?php if ($page['pagetop']): ?>
            <div class="l-page-top-wrapper">
                <div class="l-page-top-inner l-width-grid" >
                  <?php print render($page['pagetop']); ?>
                </div>
            </div>
          <?php endif; ?>

<!-- END PAGE TOP REGION -->

<!-- HEADER REGION -->

          <div class="l-header-wrapper">
            <header class="l-header-inner l-width-grid" role="banner">

            <a id="responsive-menu-button" href="#responsive-menu">MENU</a>
            <div class="l-branding">
                <?php if ($logo): ?>
                  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="site-logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
                <?php endif; ?>

                <?php if ($site_name || $site_slogan): ?>
                  <div class="site-name-slogan-wrap"> 
                  <?php if ($site_name): ?>
                    <h1 class="site-name">
                      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
                    </h1>
                  <?php endif; ?>

                  <?php if ($site_slogan): ?>
                    <h2 class="site-slogan"><?php print $site_slogan; ?></h2>
                  <?php endif; ?>

                  </div>
                <?php endif; ?>
              </div>
                <div id="responsive-menu">
                          <?php print render($page['navigation']); ?>
                </div>

            </header>
          </div>

<!-- END PAGE TOP REGION -->

<!-- MESSAGES REGION -->

         <?php print $messages; ?>

<!-- END MESSAGES REGION -->

<!-- HIGHLIGHTED REGION -->

          <?php if ($page['highlighted']): ?>
            <div class="l-highlighted-wrapper">
                <div class="l-highlighted-inner l-width-grid">
                     <?php print render($page['highlighted']); ?>
                </div>
            </div>
          <?php endif; ?>

<!-- END HIGHLIGHTED REGION -->


<!-- MAIN REGION REGION -->

          <div class="l-main-wrapper">
              <div class="l-main-inner l-width-grid">
                <div class="l-content" role="main">
                  <?php print render($page['content_top']); ?>
                  <?php print $breadcrumb; ?>
                  <a id="main-content"></a>
                  <?php print render($title_prefix); ?>
                  <?php if ($title): ?>
                    <h1><?php print $title; ?></h1>
                  <?php endif; ?>
                  <?php print render($title_suffix); ?>
                  <?php print render($tabs); ?>
                  <?php print render($page['help']); ?>
                  <?php if ($action_links): ?>
                    <ul class="action-links"><?php print render($action_links); ?></ul>
                  <?php endif; ?>
                  <?php print render($page['content']); ?>
                  <?php print $feed_icons; ?>
                </div>

                <?php print render($page['sidebar_first']); ?>

              </div>
          </div>

<!-- END MAIN REGION REGION -->


<!-- FOOTER REGION -->

        <?php if ($page['footer']): ?>
        <div class="l-footer-wrapper">
            <footer class="l-footer-inner l-width-grid" role="contentinfo">
              <?php print render($page['footer']); ?>
            </footer>
        </div>
        <?php endif; ?>

<!-- END FOOTER REGION -->

<!-- PAGE BOTTOM REGION -->
  <?php if ($page['pagebottom']): ?>

        <div class="l-page-bottom-wrapper">
            <div class="l-page-bottom-inner l-width-grid">
              <?php print render($page['pagebottom']); ?>
            </div>
        </div>

  <?php endif; ?>
<!-- END PAGE BOTTOM REGION -->

</div>

